interface RegistrationResponse {
  message: string;
}

export const createSubmitHandler =
  (
    endpoint: string,
    payload: object,
    onSuccess: () => void,
    onError: (error: string) => void,
    setIsLoading: (isLoading: boolean) => void,
    setErrorMessage: (errorMessage: string) => void
  ) =>
  async (e: React.FormEvent) => {
    setIsLoading(true);
    setErrorMessage("");
    e.preventDefault();

    try {
      const response = await fetch(endpoint, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
      });

      if (response.ok) {
        const successData: RegistrationResponse = await response.json();
        onSuccess();
      } else {
        const errorData: RegistrationResponse = await response.json();
        onError(errorData.message);
      }
    } catch {
      console.log("it triggers");
      onError("An error occurred while fetching data.");
    }

    setIsLoading(false);
  };
