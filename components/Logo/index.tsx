import Link from "next/link";
import Image from "@/components/Image";

type TestProps = {
    className?: string;
    dark?: boolean;
};

const Test = ({ className, dark }: TestProps) => (
    <Link className={`flex ${className}`} href="/">
        <Image
            src={dark ? "/images/logo-dark.svg" : "/images/logo.svg"}
            width={190}
            height={40}
            alt="Brainwave"
        />
    </Link>
);

export default Test;
