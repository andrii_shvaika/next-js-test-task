import React, { useState } from "react";
import { useRouter } from "next/navigation";
import { useColorMode } from "@chakra-ui/color-mode";
import { createSubmitHandler } from "funcs/requestHandler";
import VerificationInput from "react-verification-input";
import Image from "@/components/Image";
import Logo from "@/components/Logo";

const VerificationPage = () => {
  const { colorMode } = useColorMode();
  const [errorMessage, setErrorMessage] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [inputVal, setInputVal] = useState("");
  const isLightMode = colorMode === "light";
  const router = useRouter();

  const handleVerificationSuccess = () => {
    router.push("/homepage");
  };

  const handleVerificationError = (error: string) => {
    setErrorMessage(error);
  };

  const handleSubmit = (e: React.FormEvent) => {
    createSubmitHandler(
      "api/auth/login",
      {},
      handleVerificationSuccess,
      handleVerificationError,
      setIsLoading,
      setErrorMessage
    )(e);
  };

  return (
    <div className="relative flex min-h-screen min-h-screen-ios lg:p-6 md:px-6 md:pt-16 md:pb-10">
      <div className="relative shrink-0 w-[40rem] p-20 overflow-hidden 2xl:w-[37.5rem] xl:w-[30rem] xl:p-10 lg:hidden">
        <div className="max-w-[25.4rem]">
          <div className="mb-4 h2 text-n-1">Unlock the power of AI</div>
          <div className="body1 text-n-3">
            Chat with the smartest AI - Experience the power of AI with us
          </div>
        </div>
        <div className="absolute top-52 left-5 right-5 h-[50rem] xl:top-24">
          <Image
            className="object-contain"
            src="/images/create-pic.png"
            priority={true}
            fill
            sizes="(max-width: 1180px) 50vw, 33vw"
            alt=""
          />
        </div>
      </div>
      <div className="flex grow my-6 mr-6 p-10 bg-n-1 rounded-[1.25rem] lg:m-0 md:p-0 dark:bg-n-6">
        <div className="flex flex-col items-center w-full max-w-[31.5rem] m-auto">
          <Logo
            className="max-w-[11.875rem] mx-auto mb-10"
            dark={isLightMode}
          />
          <p className="mb-4 font-semibold text-zinc-500">
            Enter the 6-digit verification code sent to your email
          </p>
          <VerificationInput
            placeholder=""
            onChange={(e) => setInputVal(e)}
            classNames={{
              container: "h-16",
              character:
                "flex items-center justify-center rounded-md border-stone-300 shadow text-3xl",
              characterInactive: "bg-white",
              characterSelected: "text-inherit",
            }}
          />
          {errorMessage && (
            <div className="mt-4 text-red-500">{errorMessage}</div>
          )}
          <button
            onClick={handleSubmit}
            className="btn-blue btn-large w-full disabled:opacity-25 mt-4"
            disabled={inputVal.length !== 6 || isLoading}
            type="submit"
          >
            {isLoading && (
              <Image
                className="spin-animation mr-2"
                src="images/loader.svg"
                alt="Loader"
                width={15}
                height={15}
                priority={true}
              />
            )}
            Submit Verification Code
          </button>
        </div>
      </div>
    </div>
  );
};

export default VerificationPage;
