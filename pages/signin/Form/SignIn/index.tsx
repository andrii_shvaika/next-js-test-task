import React, { useState, ChangeEvent } from "react";
import { useRouter } from "next/navigation";
import Field from "@/components/Field";
import { createSubmitHandler } from "funcs/requestHandler";
import Image from "next/image";

const SignIn = () => {
  const [email, setEmail] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const router = useRouter();

  const handleVerificationSuccess = () => {
    router.push("/verification");
  };

  const handleVerificationError = (error: string) => {
    setErrorMessage(error);
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    createSubmitHandler(
      "/api/auth/request-verification",
      { email },
      handleVerificationSuccess,
      handleVerificationError,
      setIsLoading,
      setErrorMessage
    )(e);
  };

  return (
    <form onSubmit={handleSubmit}>
      <Field
        className="mb-4"
        classInput="dark:bg-n-7 dark:border-n-7 dark:focus:bg-transparent"
        placeholder="Email"
        icon="email"
        value={email}
        required
        onChange={(e: ChangeEvent<HTMLInputElement>) => {
          setEmail(e.target.value);
        }}
      />
      {errorMessage && <div className="mb-4 text-red-500">{errorMessage}</div>}
      <button
        className="btn-blue btn-large w-full disabled:opacity-25"
        disabled={!email.length || isLoading}
        type="submit"
      >
        {isLoading && (
          <Image
            className="spin-animation mr-2"
            src="images/loader.svg"
            alt="Loader"
            width={15}
            height={15}
            priority={true}
          />
        )}
        Sign in with Brainwave
      </button>
    </form>
  );
};

export default SignIn;
