import { NextApiRequest, NextApiResponse } from "next";
import { serialize } from "cookie";

const setCookies = (
  res: NextApiResponse,
  cookies: {
    name: string;
    value: string;
    maxAge?: number;
    httpOnly?: boolean;
  }[]
) => {
  const cookieStrings = cookies.map((cookie) => {
    const { name, value, maxAge = 3600, httpOnly = true } = cookie;
    return serialize(name, value, {
      maxAge,
      path: "/",
      httpOnly,
      sameSite: "lax",
      secure: true,
    });
  });

  res.setHeader("Set-Cookie", cookieStrings);
};

export default (req: NextApiRequest, res: NextApiResponse) => {
  const accessTokenVal =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";
  const refreshTokenVal = "89213120-ecb3-4a96-b9b6-3ease123124235423";

  setCookies(res, [
    { name: "accessToken", value: accessTokenVal, httpOnly: false },
    { name: "refreshToken", value: refreshTokenVal, httpOnly: false },
  ]);

  res.status(200).json({ message: "Alexi Laiho" });
};
