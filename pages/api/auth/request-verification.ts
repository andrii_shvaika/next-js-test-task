import { NextApiRequest, NextApiResponse } from "next";

const validEmailDomains = [
  "example.com",
  "mycompany.com",
  "anothercompany.com",
  "gmail.com",
];

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "POST") {
    const { email } = req.body;

    if (!email) {
      res.status(400).json({ message: "Email is required" });
      return;
    }

    const [, domain] = email.split("@");

    if (validEmailDomains.includes(domain)) {
      res.status(200).json({ message: "Registration successful" });
    } else {
      res.status(400).json({ message: "Invalid email domain" });
    }
  } else {
    res.status(405).json({ message: "Method not allowed" });
  }
}
