import React, { useState, useEffect } from "react";
import Cookies from "js-cookie";
import { useRouter } from "next/router";
import Image from "@/components/Image";
import jwtDecode from "jwt-decode";

interface JwtPayload {
  name: string;
}

const HomePage = () => {
  const [user, setUser] = useState("");
  const router = useRouter();

  useEffect(() => {
    let isMounted = true;

    const checkAccessToken = async () => {
      const cookieValue = Cookies.get("accessToken");

      if (cookieValue) {
        const decodedValue: JwtPayload = jwtDecode(cookieValue);

        if (decodedValue.name && isMounted) {
          setUser(decodedValue.name);
        }
      } else {
        await new Promise((resolve) => setTimeout(resolve, 100));

        if (isMounted && !user) {
          router.push("/");
        }
      }
    };

    checkAccessToken();

    return () => {
      isMounted = false;
    };
  }, [user, router]);

  return (
    <div className="relative flex min-h-screen min-h-screen-ios lg:p-6 md:px-6 md:pt-16 md:pb-10">
      <div className="relative shrink-0 w-[40rem] p-20 overflow-hidden 2xl:w-[37.5rem] xl:w-[30rem] xl:p-10 lg:hidden">
        <div className="max-w-[25.4rem]">
          <div className="mb-4 h2 text-n-1">Unlock the power of AI</div>
          <div className="body1 text-n-3">
            Chat with the smartest AI - Experience the power of AI with us
          </div>
        </div>
        <div className="absolute top-52 left-5 right-5 h-[50rem] xl:top-24">
          <Image
            className="object-contain"
            src="/images/create-pic.png"
            priority={true}
            fill
            sizes="(max-width: 1180px) 50vw, 33vw"
            alt=""
          />
        </div>
      </div>
      <div className="flex grow my-6 mr-6 p-10 bg-n-1 rounded-[1.25rem] lg:m-0 md:p-0 dark:bg-n-6">
        {user}
      </div>
    </div>
  );
};

export default HomePage;
