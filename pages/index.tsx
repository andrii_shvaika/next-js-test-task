import type { NextPage } from "next";
import SignInPage from "./signin";

const SignIn: NextPage = () => {
  return <SignInPage />;
};

export default SignIn;
